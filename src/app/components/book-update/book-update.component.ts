import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

import { Book } from './../../model/book';
import { BookService } from './../../service/book.service';


@Component({
  selector: 'app-book-update',
  templateUrl: './book-update.component.html',
  styleUrls: ['./book-update.component.css']
})
export class BookUpdateComponent implements OnInit {

public book: Book;

  constructor(private bookService: BookService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      const index = parseInt(params.get('id'), 0);
      this.bookService.getBookById(index)
        .subscribe(book => this.book = book);
    }, (error) => {
      console.error(error);
      this.router.navigate(['/error']);
    });
  }

  update() {
    this.bookService.updateBook(this.book).subscribe(() => {
       this.router.navigate(['/list']);
    }, (error) => {
      this.router.navigate(['/error']);
    });
  }

}
