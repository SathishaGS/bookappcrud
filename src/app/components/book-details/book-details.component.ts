import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { BookService } from './../../service/book.service';
import { Book } from './../../model/book';


@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {

  public book: Book;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private bookService: BookService) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      const index = parseInt(params.get('id'), 0);
      this.bookService.getBookById(index)
        .subscribe(task => this.book = task);
    }, (error) => {
      console.error(error);
      this.router.navigate(['/error']);
    });
  }

}
