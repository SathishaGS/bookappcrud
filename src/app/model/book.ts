export class Book {
    constructor(
       public id: number,
       public isbn: string,
       public title: string,
       public author: string,
       public description: string
    ) { }
}
