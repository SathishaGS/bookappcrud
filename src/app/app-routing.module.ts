import { ErrorComponent } from './components/error/error.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookFormComponent } from './components/book-form/book-form.component';
import { BookListComponent } from './components/book-list/book-list.component';
import { BookUpdateComponent } from './components/book-update/book-update.component';
import { BookDetailsComponent } from './components/book-details/book-details.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';


const routes: Routes = [
  {path : '', redirectTo: '/list', pathMatch : 'full'},
  {path: 'list', component: BookListComponent},
  {path: 'edit/:id', component: BookUpdateComponent},
  {path: 'add', component: BookFormComponent},
  {path: 'details/:id', component: BookDetailsComponent},
  {path: 'error', component: ErrorComponent},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
